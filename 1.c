//Write a C program to reverse a sentence entered by user
#include<stdio.h>
int main()
{
    char sen[1000] , rev[1000];
    int n=0 , i , j;
    printf("Enter a sentence :\n");
    gets(sen);
    while (sen[n] != '\0')
        {
            n++ ;
        }
    j=n-1;
    printf("\nReverse sentence:\n");
    for(i=0;i<n;i++)
    {
        rev[i]=sen[j];
        j-- ;
    }
    rev[i]='\0';
    printf("%s",rev);
    return 0;
}
