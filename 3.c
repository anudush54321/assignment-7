//Write a program to add an multiply two given matrixes
#include <stdio.h>
int main()
{
    int mat1[10][10],mat2[10][10],mulans[100][100],addans[100][100];
    int row1 , col1 , row2 , col2 , n1 , n2 ;
    printf("Enter no. of rows and columns in first matrix :\n");
    scanf("%d%d",&row1,&col1);
    printf("Enter no. of rows and columns in second matrix :\n");
    scanf("%d%d",&row2,&col2);

    if(row1 != row2 || col1 != col2)
    {
        printf("\n----- Matrix addition is not possible -----\n");
    }
    else if(row1 != col2)
    {
        printf("\n----- Matrix multiplication is not possible -----\n");
    }
    else
    {

        printf("Enter elements of first matrix :\n");
        for(int i=0 ; i<row1 ; i++)
        {
            for(int j=0 ; j<col1 ; j++)
            {
                scanf("%d",&mat1[i][j]);
            }
        }
        printf("\nEnter elements of second matrix :\n");
        for(int i=0 ; i<row2 ; i++)
        {
            for(int j=0 ; j<col2 ; j++)
            {
                scanf("%d",&mat2[i][j]);
            }
        }
        //Addition
        printf("\nResult of matrix addition :\n");
        for(int i=0 ; i<row2 ; i++)
        {
            for(int j=0 ; j<col2 ; j++)
            {
                addans[i][j] = mat1[i][j] + mat2[i][j];
                printf("%d\t",addans[i][j]);
            }
            printf("\n");
        }
        //Multiplication
        printf("\nResult of matrix multiplication :\n");
        for(int i=0 ; i<row2 ; i++)
        {
            for(int j=0 ; j<col2 ; j++)
            {
                for(int k=0 ; k<row2 ; k++)
                {
                    mulans[i][j] += mat1[i][k] * mat2[k][j];
                }
                printf("%d\t",mulans[i][j]);
            }
            printf("\n");
        }

    }
    return 0;
}
